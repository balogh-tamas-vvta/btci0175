package T6.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

@DefaultUrl("https://www.cs.ubbcluj.ro/apps/ftp/index.php")
public class NewDir extends PageObject {

    @FindBy(id="smallbutton")
    private WebElementFacade smallbutton;

    @FindBy(name="newNames[1]")
    private WebElementFacade newdirname;

    @FindBy(xpath="//*[@id=\"NewDirForm\"]/a[2]/img")
    private WebElementFacade pipe;

    @FindBy(xpath="//*[@id=\"NewDirForm\"]/a/img")
    private WebElementFacade back;

    public void click_pipe(){
        pipe.click();
    }

    public void click_back(){
        back.click();
    }

    public void click_newdir(){
        smallbutton.click();
    }

    public void enter_name(String name) {
        newdirname.type(name);
    }




    public List<String> get_new_dir() {
        WebElementFacade definitionList = find(By.tagName("td"));
        return definitionList.findElements(By.tagName("div")).stream()
                .map( element -> element.getText() )
                .collect(Collectors.toList());
    }


}