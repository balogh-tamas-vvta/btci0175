package T6.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

@DefaultUrl("https://www.cs.ubbcluj.ro/apps/ftp/index.php")
public class DeleteDir extends PageObject {

    @FindBy(id = "maintable")
    private WebElementFacade listOfDirectories;
    private void click_element_by_attribute_value(WebElementFacade el, String attribute, String value) {
        List<WebElement> list = el.findElements(By.tagName("input"));
        for (int i = 0; i<list.size(); i++){
            String dir = list.get(i).getAttribute(attribute);
            if (dir.equalsIgnoreCase(value)) {
                list.get(i).click();
                break;
            }
        }
    }


    @FindBy(xpath="/html/body/div/div[2]/div/form/table[1]/tbody/tr/td/div/input[3]")
    private WebElementFacade delete_button;

    @FindBy(xpath="//*[@id=\"CopyMoveDeleteForm\"]/a[2]/img")
    private WebElementFacade pipe;

    @FindBy(xpath="//*[@id=\"CopyMoveDeleteForm\"]/a/img")
    private WebElementFacade back;

    public void check_directory_to_delete(String directory) {
        click_element_by_attribute_value(listOfDirectories, "value", directory);
    }

    public void click_delete_button(){
        delete_button.click();
    }

    public void confirm_delete(){
        pipe.click();
    }

    public void click_back(){
        back.click();
    }



}