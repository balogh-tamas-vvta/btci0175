package T6.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

@DefaultUrl("https://www.cs.ubbcluj.ro/apps/ftp/index.php")
public class dirPage extends PageObject {

    @FindBy(name="directory")
    private WebElementFacade directory;

    @FindBy(xpath = "//*[@id=\"StatusbarForm\"]/a[4]/img")
    private WebElementFacade logoutbutton;

    public void click_logout(){
        logoutbutton.click();
    }

    public String get_current_dir(){
        return directory.getText();
    }

    public List<String> get_dir_tree() {
        WebElementFacade definitionList = find(By.tagName("div"));
        return definitionList.findElements(By.tagName("a")).stream()
                .map( element -> element.getText() )
                .collect(Collectors.toList());
    }


}