package T6.steps.serenity;

import T6.pages.DictionaryPage;
import T6.pages.LoginPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class EndUserLoginSteps {

    LoginPage loginP;

    @Step
    public void goto_home_ftppage() {
        loginP.open();
    }

    @Step
    public void enter_data_and_click_login(String server, String name, String pass) {

        loginP.click_login();
        loginP.select_server(server);
        loginP.enter_name(name);
        loginP.enter_password(pass);
        loginP.click_login();
    }


}