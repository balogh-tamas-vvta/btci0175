package T6.steps.serenity;

import T6.pages.DeleteDir;
import T6.pages.NewDir;
import net.thucydides.core.annotations.Step;

public class EndUserDeleteDirSteps {

    DeleteDir deleteDir;

    @Step
    public void delete_dir(String name){
        deleteDir.check_directory_to_delete("testbtci");
        deleteDir.click_delete_button();
        deleteDir.confirm_delete();
        deleteDir.click_back();

    }
    @Step
    public void select_directory_from_table(String directoryToDelete){
        deleteDir.check_directory_to_delete(directoryToDelete);
    }
}
