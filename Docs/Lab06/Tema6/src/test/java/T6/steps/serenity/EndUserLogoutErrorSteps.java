package T6.steps.serenity;

import T6.pages.ErrorPage;
import T6.pages.LogoutPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class EndUserLogoutErrorSteps {
    ErrorPage errorP;

    @Step
    public void verify_message(String message){
        assertThat(errorP.get_error_message(), hasItem(containsString(message)));
    }


}