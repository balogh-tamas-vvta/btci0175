package T6.steps.serenity;

import T6.pages.dirPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class EndUserDirSteps {
    dirPage dirP;

    @Step
    public void verify_user(String name){
        assert(dirP.get_current_dir().endsWith(name));
    }
    @Step
    public void verify_dir(String dir){
        assertThat(dirP.get_dir_tree(), hasItem(containsString(dir)));
    }

    @Step
    public void click_logout(){
        dirP.click_logout();
    }


}