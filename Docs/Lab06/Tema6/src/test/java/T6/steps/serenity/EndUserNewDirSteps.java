package T6.steps.serenity;

import T6.pages.NewDir;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class EndUserNewDirSteps {

    NewDir newDir;

    @Step
    public void create_new_dir(String name){
        newDir.click_newdir();
        newDir.enter_name(name);
        newDir.click_pipe();
        newDir.click_back();

    }
}
