package T6.features.search;

import T6.pages.DeleteDir;
import T6.pages.NewDir;
import T6.steps.serenity.*;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

//@RunWith(SerenityParameterizedRunner.class)
//@UseTestDataFrom("src/test/resources/features/search/login.csv")
@RunWith(SerenityRunner.class)
public class ftpStory {

    String server, name, pass;

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserLoginSteps logstep;

    @Steps
    public DeleteDir deleteFolder;

    @Steps
    public EndUserDirSteps dirstep;

    @Steps
    public EndUserLogoutSteps logoutstep;

    @Steps
    public EndUserNewDirSteps newdir;

    @Steps
    EndUserDeleteDirSteps deleteDirSteps;

    @Issue("Login valid")
    @Test
    public void login_valid() {
        logstep.goto_home_ftppage();
        logstep.enter_data_and_click_login("linux.scs.ubbcluj.ro","vvta","VVTA2020");
        newdir.create_new_dir("testbtci");
        deleteDirSteps.delete_dir("testbtci");
        dirstep.click_logout();
        logoutstep.verify_message("logged out");


    }


    /*@Issue("Login invalid")
    @Test
    public void login_invalid() {
        logstep.goto_home_ftppage();
        logstep.enter_data_and_click_login(server,name,pass);
        logouterrorstep.verify_message("error");

    }
*/
} 