<html xml:lang="en" dir="ltr"><head>
<meta http-equiv="Content-type" content="text/html;charset=iso-8859-1">
<meta name="keywords" content="net2ftp, web, ftp, based, web-based, xftp, client, PHP, SSL, SSH, SSH2, password, server, free, gnu, gpl, gnu/gpl, net, net to ftp, netftp, connect, user, gui, interface, web2ftp, edit, editor, online, code, php, upload, download, copy, move, delete, zip, tar, unzip, untar, recursive, rename, chmod, syntax, highlighting, host, hosting, ISP, webserver, plan, bandwidth">
<meta name="description" content="net2ftp is a web based FTP and SSH client. It is mainly aimed at managing websites using a browser. Edit code, upload/download files, copy/move/delete directories recursively, rename files and directories -- without installing any software.">
<link rel="shortcut icon" href="favicon.ico">
<link rel="apple-touch-icon" href="favicon.png">
<title>net2ftp - a web based FTP client</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

			<!-- /skins/skins.inc.php javascript -->
			<script type="text/javascript" src="skins/shinra/js/jquery-1.5.1.min.js"></script>
			<script type="text/javascript" src="skins/shinra/js/jquery-ui-1.8.13.custom.min.js"></script>
			<script type="text/javascript" src="skins/shinra/js/custom.js"></script>
			<script type="text/javascript" src="skins/shinra/js/superfish-1.4.8/js/hoverIntent.js"></script>
			<script type="text/javascript" src="skins/shinra/js/superfish-1.4.8/js/superfish.js"></script>
			<script type="text/javascript" src="skins/shinra/js/superfish-1.4.8/js/supersubs.js"></script>
			<script type="text/javascript" src="skins/shinra/js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
		
			<!-- /skins/skins.inc.php css -->
			<link rel="stylesheet" href="skins/shinra/css/main_desktop.ltr.css" type="text/css" media="screen">
			<link rel="stylesheet" href="skins/shinra/skins/glossy/style.css" type="text/css" media="screen">
			<link rel="stylesheet" href="skins/shinra/js/poshytip-1.0/src/tip-twitter/tip-twitter.css" type="text/css">
			<link rel="stylesheet" href="skins/shinra/js/poshytip-1.0/src/tip-yellowsimple/tip-yellowsimple.css" type="text/css">
		<style id="poshytip-css-tip-twitter" type="text/css">div.tip-twitter{visibility:hidden;position:absolute;top:0;left:0;}div.tip-twitter table, div.tip-twitter td{margin:0;font-family:inherit;font-size:inherit;font-weight:inherit;font-style:inherit;font-variant:inherit;}div.tip-twitter td.tip-bg-image span{display:block;font:1px/1px sans-serif;height:10px;width:10px;overflow:hidden;}div.tip-twitter td.tip-right{background-position:100% 0;}div.tip-twitter td.tip-bottom{background-position:100% 100%;}div.tip-twitter td.tip-left{background-position:0 100%;}div.tip-twitter div.tip-inner{background-position:-10px -10px;}div.tip-twitter div.tip-arrow{visibility:hidden;position:absolute;overflow:hidden;font:1px/1px sans-serif;}</style><style id="poshytip-css-tip-yellowsimple" type="text/css">div.tip-yellowsimple{visibility:hidden;position:absolute;top:0;left:0;}div.tip-yellowsimple table, div.tip-yellowsimple td{margin:0;font-family:inherit;font-size:inherit;font-weight:inherit;font-style:inherit;font-variant:inherit;}div.tip-yellowsimple td.tip-bg-image span{display:block;font:1px/1px sans-serif;height:10px;width:10px;overflow:hidden;}div.tip-yellowsimple td.tip-right{background-position:100% 0;}div.tip-yellowsimple td.tip-bottom{background-position:100% 100%;}div.tip-yellowsimple td.tip-left{background-position:0 100%;}div.tip-yellowsimple div.tip-inner{background-position:-10px -10px;}div.tip-yellowsimple div.tip-arrow{visibility:hidden;position:absolute;overflow:hidden;font:1px/1px sans-serif;}</style></head>
<body onload="">

<!-- Template /skins/shinra/consent.template.php begin -->

<form id="LoginForm1" action="/apps/ftp/index.php" method="post">
<input type="hidden" name="state" value="login">
<input type="hidden" name="consent_necessary" value="1">
<input type="hidden" name="consent_nonpersonalized_ads" value="1">



<div id="consent">

<h6>This website uses cookies</h6>

We use cookies to personalise content and ads, to provide social media features and to analyse our traffic.
We also share information about your use of our site with our social media, advertising and analytics partners who may combine it with other information that you've provided to them or that they've collected from your use of their services. 
You consent to our cookies if you continue to use this website. <br>

<br>

<input type="checkbox" name="consent_necessary" value="1" checked="checked" disabled="disabled" style="width: 15px;"> 
	Necessary            <br>
<input type="checkbox" name="consent_preferences" value="1" checked="checked" style="width: 15px;">
	Preferences          <br>
<input type="checkbox" name="consent_statistics" value="1" checked="checked" style="width: 15px;">
	Statistics           <br>
<input type="checkbox" name="consent_personalized_ads" value="1" style="width: 15px;" title="Google considers ads to be personalized when they are based on previously collected or historical data to determine or influence ad selection, including a user's previous search queries, activity, visits to sites or apps, demographic information, or location. Specifically, this would include, for example: demographic targeting, interest category targeting, remarketing, targeting Customer Match lists, and targeting audience lists uploaded in Display &amp; Video 360 or Campaign Manager."> 
	Personalized ads<br>
<input type="checkbox" name="consent_nonpersonalized_ads" value="1" checked="checked" disabled="disabled" style="width: 15px;" title="Non-personalized ads are ads that are not based on a user’s past behavior. They are targeted using contextual information, including coarse (such as city-level, but not ZIP/postal code) geo-targeting based on current location, and content on the current site or app or current query terms. Although non-personalized ads don’t use cookies or mobile ad identifiers for ad targeting, they do still use cookies or mobile ad identifiers for frequency capping, aggregated ad reporting, and to combat fraud and abuse."> 
	Non-personalized ads<br>

<br>

<input type="submit" id="LoginButton1" name="Save" value="Save cookie choice" alt="Save cookie choice" style="width: 80%; margin-left: 10%; margin-right: 10%;">
</div>

</form>
<!-- Template /skins/shinra/consent.template.php end -->


</body></html>