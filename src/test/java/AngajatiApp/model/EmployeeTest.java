package AngajatiApp.model;

import AngajatiApp.validator.EmployeeException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class EmployeeTest {
    Employee e1, e2, e3;

    @BeforeEach
    public void SetUp() {
        e1 = new Employee();
        e2 = null;
        e3 = new Employee();
        e3.setFirstName("Balogh");
        e1.setFirstName("");
        System.out.println("Before Test");

    }

    @Test
    @Order(2)
    void getFirstName() {
        assertEquals("Balogh", e3.getFirstName());
        assertEquals("", e1.getFirstName());
        System.out.println("e3 has got First Name Balogh ");

    }

    @Test
    @Order(1)
    void getFirstName2() {
        try {
            assertEquals("", e2.getFirstName());
            assert (false);
        } catch (Exception e) {
            System.out.println(e.toString());
            assert (true);
        }
        System.out.println("e2 doesn't have first name");
    }

    @Test
    @Order(3)
    void setFirstName() {
        assertEquals("", e1.getFirstName());
        e1.setFirstName("Liviu");
        assertNotEquals("", e1.getFirstName());
        assertEquals("Liviu", e1.getFirstName());
        System.out.println("setFirstName ok");

    }

    @Test
    @Order(4)
    void constructorEmployee() {

        Employee tmp = new Employee();
        assertEquals(e1.getFirstName(), tmp.getFirstName());
        assertNotEquals(null, tmp);
        System.out.println("Constructor test ok");

    }


    @Test
    @Order(6)
    void getFirstName3() {
        assertEquals("Balogh", e3.getFirstName());
        assertEquals("", e1.getFirstName());
        try {
            assertEquals("", e2.getFirstName());
        } catch (Exception e) {
            assert (true);
        }

        System.out.println("getFirstName tests ok");
    }

    @Disabled
    @Test
    @Order(5)
    void getFirstName_Disabled() {
        assertEquals("ASDF", e1.getFirstName());
        System.out.println("e1 hasn't got FirstName");
    }

    @Test
    @Order(8)
    void getEmployeeFromStringTest() {

        e1.setCnp("123442");

        Exception exception = assertThrows(EmployeeException.class, () -> {
            Employee.getEmployeeFromString(e1.toString(), 3);
        });

        String expectedMessage = "Invalid line at: 3";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @ParameterizedTest
    @Order(7)
    @CsvSource({"asd", "!!!"})
    void getEmployeeFromStringTestParametrized(String cnp) {

        e1.setCnp(cnp);

        Exception exception = assertThrows(EmployeeException.class, () -> {
            Employee.getEmployeeFromString(e1.toString(), 3);
        });

        String expectedMessage = "Invalid line at: 3";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

    }

    @AfterEach
    public void TearDown() {
        e1 = null;
        e1 = null;
        e1 = null;
        System.out.println("After Test");
    }

    //timeout: varianta cu adnotarea Timeout
    @Test
    //@Timeout(value = 100, unit = TimeUnit.MILLISECONDS)
    @Timeout(value = 2, unit = TimeUnit.SECONDS)
    //@Timeout(5)
    public void findByFirstName() {
        try {
            //Thread.sleep(100);
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //nu avem nici un autor pentru cartea c1
        assertEquals(e1.getFirstName(),"");
        System.out.println("search with timeout ok");
    }

    //timeout: varianta cu metoda asserTimeout
    @Test
    public void cautaDupaAutor_cu_assertTimeout() {
        Assertions.assertTimeout(Duration.ofSeconds(10), () -> {
            delay_and_call();
        }, ("")); //Nu avem nici un autor pt. c1
        System.out.println("search with assertTimeout ok");
    }

    private void delay_and_call() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ;
        e1.getFirstName();
    }


}