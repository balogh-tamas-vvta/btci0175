package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {
    EmployeeMock em;

    @BeforeEach
    void setUp() {
        em = new EmployeeMock();
    }

    @After
    public void tearDown() {
        em = null;
    }

@Test
    void addEmployee_TC1() {
        Employee e = new Employee();
        e.setId(1);
        e.setFirstName("Marian");
        e.setLastName("Popescu");
        e.setCnp("56");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(5.000);
        //int nr_employee=em.getEmployeeList().size();
        //em.addEmployee(e);
        //verificam daca intr-adevar angajatul s-a adaugat cu succes
        //assertEquals(nr_angajati+1,em.getEmployeeList().size());
        assertEquals(false, em.addEmployee(e));
    }

    @Test
    void addEmployee_TC2() {
        Employee e = new Employee();
        e.setId(2);
        e.setFirstName("");
        e.setLastName("Cristina");
        e.setCnp("1997865436790");
        e.setFunction(DidacticFunction.TEACHER);
        e.setSalary(25.6);
        assertEquals(false, em.addEmployee(e));


    }

    @Test
    public void addEmployee_TC3() {
        Employee e = new Employee();
        e.setId(3);
        e.setFirstName("H");
        e.setLastName("Ioana");
        e.setCnp("9876543217654");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setSalary(55.6);
        assertEquals(false, em.addEmployee(e));

    }


    @Test
    public void addEmployee_TC4() {
        Employee e = new Employee();
        e.setId(3);
        e.setFirstName("HP");
        e.setLastName("Ionut");
        e.setCnp("9876543217654");
        e.setFunction(DidacticFunction.TEACHER);
        e.setSalary(55.6);
        assertEquals(false, em.addEmployee(e));

    }

    @Test
    public void addEmployee_TC5() {
        Employee e = new Employee();
        e.setId(3);
        e.setFirstName("MGDAUGDADUGUGSAIG");
        e.setLastName("Irina");
        e.setCnp("9876543217654");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(55.6);
        assertEquals(true, em.addEmployee(e));

    }

    @Test
    public void addEmployee_TC6() {
        Employee e = new Employee();
        e.setId(3);
        e.setFirstName("MGDAUGDADUGUGSAIGhdgjydgdgdgGAI");
        e.setLastName("Tiberiu");
        e.setCnp("9876543217654");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setSalary(55.6);
        assertEquals(true, em.addEmployee(e));

    }



    // T4

    @Test
    public void modifyEmp1() {   //TC1
        Employee e1=null;
        List<Employee> employeeMockList = em.getEmployeeList();
        em.modifyEmployeeFunction(e1,DidacticFunction.ASISTENT);
        assertTrue(em.getEmployeeList().equals(employeeMockList));
    }

    @Test
    public void modifyEmp2() {   //TC2

        Employee e1 = new Employee();
        e1.setId(1);
        e1.setLastName("Daniel");
        e1.setFirstName("Ionut");
        e1.setCnp("1234567891111");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(32.5);
        em.addEmployee(e1);

        List<Employee> employeeMockList = em.getEmployeeList();
        em.modifyEmployeeFunction(e1,DidacticFunction.LECTURER);
        assertTrue(e1.getFunction() == DidacticFunction.LECTURER);
    }


    @Test
    public void modifyEmp3() {   //TC3

        Employee e1 = new Employee();
        e1.setId(100);
        e1.setLastName("Daniel");
        e1.setFirstName("Ionut");
        e1.setCnp("1234567891111");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(32.5);
        em.addEmployee(e1);

        List<Employee> employeeMockList = em.getEmployeeList();

        em.modifyEmployeeFunction(e1, DidacticFunction.ASISTENT);
        assertTrue(em.getEmployeeList().equals(employeeMockList));
    }

}

